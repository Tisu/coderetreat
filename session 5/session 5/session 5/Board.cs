﻿using System;
using System.Threading;

namespace session_5
{
    public class Board
    {
        private const int Size = 10;
        private int[,] board;

        public Board()
        {
            board = new int[Size, Size];

            var random = new Random();
            Loop.For(0, 20, (_) =>
            {
                board[random.Next(0, Size), random.Next(0, Size)] = 1;
            });
            Print();
        }

        public void Play()
        {
            Loop.For(0, 10, (_) =>
             {
                 Recalculate();
                 Print();
                 Thread.Sleep(TimeSpan.FromSeconds(3));
             });
        }

        public void Print()
        {
            Loop.For(0, Size, (i) =>
            {
                string lineBuffor = "";
                Loop.For(0, Size, (j) =>
                {
                    lineBuffor += board[i, j] == 1 ? '1' : ' ';
                });
                Console.WriteLine(lineBuffor);
            });
            Console.WriteLine("------------------------------------");
        }

        private void Recalculate()
        {
            var tempBoard = new int[Size, Size];
            Loop.For(0, Size, (i) =>
            {
                Loop.For(0, Size, (j) =>
                {
                    var neigbours = CountNeighbours(i, j);
                    if ((board[i, j] == 1 && (neigbours == 2 || neigbours == 3)) || (board[i, j] == 0 && neigbours == 3))
                    {
                        tempBoard[i, j] = 1;
                    }
                    else
                    {
                        tempBoard[i, j] = 0;
                    }
                });
            });

            board = tempBoard;
        }

        public int CountNeighbours(int x, int y)
        {
            var total = 0;
            Loop.ForEqual(Math.Max(0, x - 1), Math.Min(Size - 1, x + 1), (i) =>
            {
                Loop.ForEqual(Math.Max(0, y - 1), Math.Min(Size - 1, y + 1), (j) =>
                {
                    total += board[i, j];
                });
            });
            total -= board[x, y];
            return total;
        }
    }
}