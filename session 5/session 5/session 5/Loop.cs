﻿using System;

namespace session_5
{
    public class Loop
    {
        public static void For(int min, int max, Action<int> action)
        {
            int current = min;
            ForInternal(min, max, ref current, action);
        }

        private static void ForInternal(int min, int max, ref int current, Action<int> action)
        {
            if (current == max)
            {
                return;

            }
            action(current);
            current++;

            ForInternal(min, max, ref current, action);
        }

        public static void ForEqual(int min, int max, Action<int> action)
        {
            int current = min;
            ForInternalEqual(min, max, ref current, action);
        }

        private static void ForInternalEqual(int min, int max, ref int current, Action<int> action)
        {
            action(current);
            current++;
            if (current == max + 1)
            {
                return;

            }
            ForInternalEqual(min, max, ref current, action);
        }


    }
}