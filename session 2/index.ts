
function playGame() {
    let board = initBoard(10, 10);
    initLive(board, 10, 10, 10);
    for (let i = 0; i < 10; i++) {
        evaluate(board, 10, 10);
        printBoard(board);
    }
}

function evaluate(board: number[][], x: number, y: number) {
    for (let i = 0; i < i; i++) {
        for (var j = 0; j < y; j++) {
            board[i][j] = isAlive(board, i, j);
        }
    }
}

function printBoard(board: number[][]) {
    board.forEach(x => console.log(x.join(' ')));
    console.log(' ')
}

function initBoard(x: number, y: number) {
    let board = [];
    for (let i = 0; i < x; i++) {
        board[i] = [];
        for (let j = 0; j < y; j++) {
            board[i][j] = 0;
        }
    }
    return board;
}

function initLive(board: number[][], iteration: number, x: number, y: number) {
    for (var i = 0; i < iteration; i++) {
        let posx = Math.round(Math.random() * (x - 1));
        let posy = Math.round(Math.random() * (y - 1));
        console.log(board[posx], posx, posy)
        board[posx][posy] = 1;
    }
}
function isAlive(board: number[][], x: number, y: number) {

    const n = numberOfNeighbours(board, x, y);
    const c = board[x][y];

    return (c === 1 && (n === 2 || n === 3)) || (c === 0 && n === 3) ? 1 : 0;
}
function helper(row: number[], x: number) {
    if (row == null)
        return 0;

    return row[x] + (row[x - 1] || 0) + (row[x + 1] || 0);
}
function numberOfNeighbours(board: number[][], x: number, y: number): number {
    return helper(board[x - 1], y) + helper(board[x], y) - (board[x][y]) + helper(board[x - 1], y);
}

playGame();