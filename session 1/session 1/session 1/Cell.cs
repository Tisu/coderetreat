﻿using System;

namespace session_1
{
    public class Cell
    {
        public int Y { get; set; }

        public int X { get; set; }

        public bool IsAlive { get; set; }

        public Cell Clone()
        {
            return new Cell() { X = this.X, Y = this.Y };
        }
        public override bool Equals(object obj)
        {
            var cell = obj as Cell;
            return cell != null &&
                   Y == cell.Y &&
                   X == cell.X;
        }

        public bool IsNeighbour(Cell other)
        {
            return Math.Abs(Math.Sqrt(Math.Pow(other.X - this.X, 2) + Math.Pow(other.Y - this.Y, 2)) - 1) < 0.2f;
        }
    }
}