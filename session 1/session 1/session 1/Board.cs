﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace session_1
{
    public class Board
    {
        private const int Size = 9;

        private const char DeadMark = ' ';
        private const char AliveMark = 'X';
        private List<Cell> cells;

        public Board()
        {
            this.cells = new List<Cell>();
            GenerateBoard();
        }

        private void GenerateBoard()
        {
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    var cell = new Cell
                    {
                        X = i,
                        Y = j,
                        IsAlive = IsZeroCell(i, j)
                    };

                    this.cells.Add(cell);
                }
            }
            PrintBoard();
        }

        private static bool IsZeroCell(int i, int j)
        {
            return i == 0 && j == 0;
        }

        private void PrintBoard()
        {
            for (var i = 0; i < Size; i++)
            {
                string lineBuffor = "";
                for (var j = 0; j < Size; j++)
                {
                    var currentCell = this.cells.Single(x => x.X == j && x.Y == i);

                    lineBuffor += currentCell.IsAlive ? AliveMark : DeadMark;
                }
                Console.WriteLine(lineBuffor);
            }
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine();
        }
        public void Life()
        {
            var newBoard = new List<Cell>();
            foreach (var cell in this.cells)
            {
                var newCell = cell.Clone();
                var neighbours = cells.Where(IsNeighbour(cell)).ToList();

                if (!cell.IsAlive)
                {
                    var neighboursDead = cells.Where(IsNeighbour2(cell)).ToList();
                    if (neighboursDead.Count == 3)
                    {
                        newCell.IsAlive = true;
                    }
                    newBoard.Add(newCell);
                    continue;
                }
                if (ShouldDie(neighbours))
                {
                    newCell.IsAlive = false;
                }
                newBoard.Add(newCell);
            }
            this.cells = newBoard;
            PrintBoard();
        }

        private static Func<Cell, bool> IsNeighbour(Cell cell)
        {
            return x => !x.Equals(cell) && x.IsAlive && x.IsNeighbour(cell);
        }

        private static Func<Cell, bool> IsNeighbour2(Cell cell)
        {
            return x => !x.Equals(cell) && !x.IsAlive && x.IsNeighbour(cell);
        }

        private static bool ShouldDie(List<Cell> neighbours)
        {
            return neighbours.Count < 2 || neighbours.Count > 3;
        }
    }
}