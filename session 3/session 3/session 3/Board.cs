﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;

namespace session_3
{
    public class Board
    {
        private List<Cell> board = new List<Cell>();

        public Board()
        {
            for (var i = 0; i < 10; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    board.Add(new Cell() { X = i, Y = j, IsAlive = false });
                }
            }
            var random = new Random();
            for (int i = 0; i < 20; i++)
            {
                this.board.ElementAt(random.Next(0, this.board.Count)).IsAlive = true;
            }
            Print();
        }

        public void Play()
        {
            var newBoard = new List<Cell>();
            foreach (var cell in board)
            {
                var neighbours = this.board.Where(x => x.IsAlive == true && !cell.Equals(x) && Distance(x, cell)).ToList();
                if (cell.IsAlive == false)
                {
                    if (neighbours.Count == 3)
                    {
                        var newCell = cell.Clone();
                        newCell.IsAlive = true;
                        newBoard.Add(newCell);
                        continue;
                    }
                    newBoard.Add(cell);
                    continue;
                }

                if (neighbours.Count == 2 || neighbours.Count == 3)
                {
                    newBoard.Add(cell);
                    continue;
                }

                newBoard.Add(cell.Clone());
            }

            this.board = newBoard;
            var a = this.board.Where(x => x.IsAlive == true).ToList();
            Print();
        }

        private void Print()
        {
            var lineBuffer = "";
            for (var j = 0; j < 100; j++)
            {
                var cell = this.board.ElementAt(j);
                if (j % 10 == 0 && j != 0)
                {
                    Console.WriteLine(lineBuffer);
                    lineBuffer = "";
                }
                lineBuffer += cell.IsAlive == true ? 'X' : ' ';
            }


            Console.WriteLine("--------------------------------");
            Console.WriteLine();
        }
        private bool Distance(Cell cell1, Cell cell)
        {
            return Math.Sqrt(Math.Pow(cell.X - cell1.X, 2) + Math.Pow(cell.Y - cell1.Y, 2) - 1)  < 0.2f;
        }
    }
}