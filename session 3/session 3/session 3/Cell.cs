﻿namespace session_3
{
    public class Cell
    {
        public dynamic X { get; set; }
        public dynamic Y { get; set; }
        public dynamic IsAlive { get; set; }

        public override bool Equals(object obj)
        {
            var cell = obj as Cell;
            return cell != null &&
                   System.Collections.Generic.EqualityComparer<dynamic>.Default.Equals(X, cell.X) &&
                   System.Collections.Generic.EqualityComparer<dynamic>.Default.Equals(Y, cell.Y);
        }

        public Cell Clone()
        {
            return new Cell() { X = this.X, Y = this.Y, IsAlive = false };
        }
    }
}