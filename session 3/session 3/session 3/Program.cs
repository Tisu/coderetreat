﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var board = new Board();
            for (int i = 0; i < 10; i++)
            {
                board.Play();
            }
            Console.ReadKey();
        }
    }
}
