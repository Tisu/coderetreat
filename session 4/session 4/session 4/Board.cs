﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace session_4
{
    public class Board
    {
        private int[,] board;
        private const int Size = 10;
        public Board()
        {
            board = new int[10, 10];
            GenerateBoard();
            Print();
        }

        private void GenerateBoard()
        {
            var random = new Random();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    board[i, j] = random.Next(0, 2);
                }
            }
        }

        public void Play()
        {
            while (true)
            {
                Recalculate();
                Print();
                Thread.Sleep(TimeSpan.FromSeconds(3));
            }
        }

        private void Print()
        {
            Console.WriteLine("-----------------------------------------");
            for (int i = 0; i < Size; i++)
            {
                string lineBufffor = "";
                lineBufffor = ConcatenateOneLine(lineBufffor, i);
                Console.WriteLine(lineBufffor);
            }
        }

        private string ConcatenateOneLine(string lineBufffor, int i)
        {
            for (int j = 0; j < Size; j++)
            {
                lineBufffor += board[i, j];
            }
            return lineBufffor;
        }

        public void Recalculate()
        {
            var tempBoard = new int[10, 10];
            for (int i = 0; i < Size; i++)
            {
                CalculateRow(i, tempBoard);
            }
            board = tempBoard;
        }

        private void CalculateRow(int i, int[,] tempBoard)
        {
            for (int j = 0; j < Size; j++)
            {
                var alive = AliveCells(i, j);
                tempBoard[i, j] =
                    Convert.ToInt32(board[i, j] == 1 && (alive == 2 || alive == 3) ||
                                    (board[i, j] == 0 && alive == 3));
            }
        }

        public int AliveCells(int x, int y)
        {
            int total = 0;

            for (int i = Math.Max(0, x - 1); i <= Math.Min(Size - 1, x + 1); i++)
            {
                for (int j = Math.Max(0, y - 1); j <= Math.Min(Size - 1, y + 1); j++)
                {
                    total += board[i, j];
                }
            }
            total -= board[x, y];

            return total;
        }


    }
}